# Optimization for food manufacture

Problem is borrowed from book *Model Building in Mathematical Programming* by H. Paul Williams (Fifth edition)

## Problem statement

A food is manufactured by reﬁning raw oils and blending them together. The raw oils are of two categories:

| Vegetable oils | Non-vegetable oils |
|----------------|-------|
| VEG 1 | OIL 1 |
| VEG 2 | OIL 2 |
| | OIL 3| 

Each oil may be purchased for immediate delivery (January) or bought on the futures market for delivery in a subsequent month. Prices at present and in the futures market are given below in (£/ton)

|         | VEG 1 | VEG 2 | OIL 1 | OIL 2 | OIL 3 |
|---------|-------|-------|-------|-------|-------|
|January  |  110  |   120 |  130  |  110  | 115   |
|February |  130  |   130 |  110  |  90   | 115   |
| March   |  110  |   140 |  130  |  100  | 95    |
| April   |  120  |   110 |  120  |  120  | 125   |
| May     |  100  |   120 |  150  |  110  | 105   |
| June    |  90   |   100 |  140  |  80   | 135   |

The ﬁnal product sells at £150 per ton.

Vegetable oils and non-vegetable oils require different production lines for refining. In any month, it is not possible to reﬁne more than 200 tons of vegetable oils and more than 250 tons of non-vegetable oils. There is no loss of weight in the refining process, and the cost of refining may be ignored.

It is possible to store up to 1000 tons of each raw oil for use later. The cost of storage for vegetable and non-vegetable oil is £5 per ton per month. The final product cannot be stored, nor can reﬁned oils be stored.

There is a technological restriction of hardness on the ﬁnal product. In the units in which hardness is measured, this must lie between 3 and 6. It is assumed that hardness blends linearly and that the hardnesses of the raw oils are


| Oil | Hardness $\left[\dfrac{\text{oil amount}}{\text{product}}\right]$ |
|-------|-----|
| VEG 1 | 8.8 |
| VEG 2 | 6.1 |
| OIL 1 | 2.0 |
| OIL 2 | 4.2 |
| OIL 3 | 5.0 |

What buying and manufacturing policy should the company pursue in order to maximise proﬁt?

At present, there are 500 tons of each type of raw oil in storage. It is required that these stocks will also exist at the end of June.