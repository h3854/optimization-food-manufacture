using JuMP, HiGHS

model = Model(HiGHS.Optimizer)

months = 6

oil_costs = Dict( "VEG 1" => [110,130,110,120,100,90], "VEG 2" => [120,130,140,110,120,100], "OIL 1" => [130,110,130,120,150,140],"OIL 2" => [110,90,100,120,110,80], "OIL 3" => [115,115,95,125,105,135])

hardness = Dict( "VEG 1" => 8.8, "VEG 2" => 6.1, "OIL 1" => 2.0,"OIL 2" => 4.2, "OIL 3" => 5.0)

all_oils = ["VEG 1","VEG 2","OIL 1","OIL 2","OIL 3"]
veg_oils = all_oils[1:2]
non_veg_oils = all_oils[3:5]

@variable(model, 0.0 <= x_prod[1:6])
@variable(model, 0.0 <= x_raw[ all_oils,1:6 ])
@variable(model, 0.0 <= x_stored[ all_oils,1:6 ] <= 1000.0)


@variable(model, 0.0 <= x_refined_veg[ veg_oils,1:6 ] <= 200.0)
@variable(model, 0.0 <= x_refined_oil[ non_veg_oils,1:6 ] <= 250.0)

# Storage constraints for vegetable oils
@constraint(model, [i in ["VEG 1", "VEG 2"]], x_raw[i, 1] + 500 - x_refined_veg[i, 1] == x_stored[i, 1])
@constraint(model, [i in ["VEG 1", "VEG 2"], j in 2:6], x_raw[i, j] + x_stored[i,j-1] - x_refined_veg[i, j] == x_stored[i, j])


# Storage constraints for non-vegetable oils
@constraint(model, [i in ["OIL 1", "OIL 2", "OIL 3"]], x_raw[i, 1] + 500 - x_refined_oil[i, 1] == x_stored[i, 1])
@constraint(model, [i in ["OIL 1", "OIL 2", "OIL 3"], j in 2:6], x_raw[i, j] + x_stored[i,j-1]- x_refined_oil[i, j] == x_stored[i, j])

# Constraint for fixed end amount of Storage
@constraint(model, [i in all_oils], x_stored[i,6] == 500 )

@constraint(model, [i in 1:6], x_refined_veg["VEG 1",i] + x_refined_veg["VEG 2",i] + x_refined_oil["OIL 1",i] + x_refined_oil["OIL 2",i] + x_refined_oil["OIL 3",i] == x_prod[i] )
@constraint(model, [i in 1:6], hardness["VEG 1"]*x_refined_veg["VEG 1",i] + hardness["VEG 2"]*x_refined_veg["VEG 2",i] + hardness["OIL 1"]*x_refined_oil["OIL 1",i] + hardness["OIL 2"]*x_refined_oil["OIL 2",i] + hardness["OIL 3"]*x_refined_oil["OIL 3",i] <= 6*x_prod[i] )
@constraint(model, [i in 1:6], hardness["VEG 1"]*x_refined_veg["VEG 1",i] + hardness["VEG 2"]*x_refined_veg["VEG 2",i] + hardness["OIL 1"]*x_refined_oil["OIL 1",i] + hardness["OIL 2"]*x_refined_oil["OIL 2",i] + hardness["OIL 3"]*x_refined_oil["OIL 3",i] >= 3*x_prod[i] )

@objective(model, Max, sum(150*x_prod[j]-oil_costs[i][j]*x_raw[i,j]-5*x_stored[i,j] for i in all_oils, j in 1:6))


res = optimize!(model)